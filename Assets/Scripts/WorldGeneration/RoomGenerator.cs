﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utility;

public class Room {

    public int[,] TileMap { get; set; }

    public int Width { get { return TileMap.GetLength(0); } }
    public int Height { get { return TileMap.GetLength(1); } }
    public Size MapSize { get { return new Size() { Width = this.Width, Height = this.Height }; } }

	public static Room GenerateRandomRoom(int width, int height, float obstaclePercent, int seed)
    {
        var allTileCoords = new List<Coord>();
        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                var coord = new Coord(x, y);
                allTileCoords.Add(coord);
            }
        }

        var mapCenter = new Coord(width / 2, height / 2);
        var shuffledTileCoords = new Queue<Coord>(General.ShuffleArray(allTileCoords.ToArray(), seed));

        int[,] obstacleMap = new int[width, height];
        int obstacleCount = (int)(width * height * obstaclePercent);
        int currentObstacleCount = 0;

        for (int i = 0; i < obstacleCount; i++)
        {
            var randomCoord = shuffledTileCoords.Dequeue();
            shuffledTileCoords.Enqueue(randomCoord);

            obstacleMap[randomCoord.x, randomCoord.y] = (int)RoomTileType.Obstacle;
            currentObstacleCount++;
            
            if(randomCoord == mapCenter && !MapIsFullAccesable(mapCenter, obstacleMap, currentObstacleCount))
            {
                obstacleMap[randomCoord.x, randomCoord.y] = (int)RoomTileType.Ground;
                currentObstacleCount--;
            }
        }

        var outputRoom = new Room() { TileMap = obstacleMap };

        return outputRoom;
    }

    public static bool MapIsFullAccesable(Coord mapCenter, int[,] map, int currentObstacleCount)
    {
        var mapFlags = new bool[map.GetLength(0), map.GetLength(1)];
        var queue = new Queue<Coord>();
        queue.Enqueue(mapCenter);
        mapFlags[mapCenter.x, mapCenter.y] = true;

        var accessableTileCount = 1;

        while (queue.Count > 0)
        {
            var tile = queue.Dequeue();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    var neighbourX = tile.x + x;
                    var neighbourY = tile.y + y;
                    if (x == 0 || y == 0)
                    {
                        if ((neighbourX >= 0 && neighbourX < map.GetLength(0)) && (neighbourY >= 0 && neighbourY < map.GetLength(1)))
                        {
                            if (!mapFlags[neighbourX, neighbourY] && Coord.IsCoordWalkable(map[neighbourX, neighbourY]))
                            {
                                mapFlags[neighbourX, neighbourY] = true;
                                queue.Enqueue(new Coord(neighbourX, neighbourY));
                                accessableTileCount++;
                            }
                        }
                    }
                }
            }
        }

        var targetAccessibleTileCount = (map.GetLength(0) * map.GetLength(1)) - currentObstacleCount;

        return targetAccessibleTileCount == accessableTileCount;
    }
}

public enum RoomTileType
{
    Ground = 0,
    Air = 1,
    Obstacle = 2
}

[System.Serializable]
public struct Coord
{
    public int x;
    public int y;
    public RoomTileType Tile;

    public Coord(int _x, int _y, RoomTileType _tile = RoomTileType.Ground)
    {
        x = _x;
        y = _y;
        Tile = _tile;
    }

    public bool IsWalkable()
    {
        return IsCoordWalkable((int)Tile);
    }

    public static bool IsCoordWalkable(int tileType)
    {
        return tileType == (int)RoomTileType.Ground;
    }

    public static bool operator ==(Coord c1, Coord c2)
    {
        return c1.x == c2.x && c1.y == c2.y;
    }

    public static bool operator !=(Coord c1, Coord c2)
    {
        return !(c1 == c2);
    }
}