﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Utility;

namespace RPG.Data.Statistics
{
    public class Stat
    {
         public enum Names {
            NONE,
            Health,
            Mana,
            Attack,
            Defense,
            SpecialAttack,
            SpecialDefense,
            Speed
        }
        protected int _LastKnownLevel = 1;
        public Names Name { get; protected set; }
        public int Base { get; set; }
        protected int? _current;
        public int Current
        {
            get
            {
                if (!_current.HasValue)
                    _current = CalculateCurrentStat();
                return _current.Value;
            }
            protected set { _current = value; }
        }
        protected int? _max;
        public int Max { 
            get {
                if (!_max.HasValue)
                    _max = CalculateMaxStat();
                return _max.Value;
            } 
            protected set { _max = value; } 
        }
        public int Stages { get; protected set; }
        public int IndividualValue { get; protected set; }
        public int EffortValue { get; protected set; }

        public Stat(Names name, int b)
        {
            this.Name = name;
            this.Base = b;
            this.Stages = 0;
            this.IndividualValue = 0;
            this.EffortValue = 0;
        }
        public Stat(Names name, int b, int iv, int ev)
        {
            this.Name = name;
            this.Base = b;
            this.Stages = 0;
            this.IndividualValue = iv;
            this.EffortValue = ev;
        }

        public void AddLevelAndNature(int level)
        {
            this._LastKnownLevel = level;

            Recalculate();
        }

        public virtual int CalculateMaxStat(int level)
        {
            this._LastKnownLevel = level;

            var output = (int)Math.Floor((Math.Floor(((2 * Base + IndividualValue + Math.Floor(EffortValue / 4.0)) * level) / 100) + 5));

            return output;
        }
        public virtual int CalculateMaxStat()
        {
            return CalculateMaxStat(_LastKnownLevel);
        }

        public virtual int CalculateCurrentStat(int level)
        {
            this._LastKnownLevel = level;
            var output = (int)Math.Floor(Max * GetStagesModifier());

            return output;
        }
        public virtual int CalculateCurrentStat()
        {
            return CalculateCurrentStat(_LastKnownLevel);
        }

        protected virtual double GetStagesModifier()
        {
            var stages = Stages;
            var stagesMod = 2 + ((stages < 0) ? stages * -1 : stages);

            return (stages < 0) ? 2 / stagesMod : stagesMod / 2;
        }

        public virtual int Recalculate()
        {
            var oldCurrent = Current;

            this.Max = CalculateMaxStat();
            this.Current = CalculateCurrentStat();

            return Current - oldCurrent;
        }

        public int AddLevel(int levelsAdd)
        {
            if (_LastKnownLevel + levelsAdd > Stats.MAX_LEVEL)
                _LastKnownLevel = Stats.MAX_LEVEL;
            else
                _LastKnownLevel += levelsAdd;

            return Recalculate();
        }
        public int AddLevel()
        {
            return AddLevel(1);
        }

        private readonly int MAX_PLUS_OR_MINUS_STAGES = 6;
        public bool AddStage()
        {
            if(Stages >= MAX_PLUS_OR_MINUS_STAGES)
            {
                //Send Message to Log of Fail
                return false;
            }

            Stages += 1;
            this.Current = CalculateCurrentStat();

            return true;
        }
        public bool SubtractStage()
        {
            if (Stages <= -MAX_PLUS_OR_MINUS_STAGES)
            {
                //Send Message to Log of Fail
                return false;
            }

            Stages -= 1;
            this.Current = CalculateCurrentStat();

            return true;
        }

        public void ClearStages()
        {
            Stages = 0;
        }

        public const int MIN_INDIVIDUAL_VALUE = 0;
        public const int MAX_INDIVIDUAL_VALUE = 31;
        public int SetIndividualValue(int value)
        {
            if(value < MIN_INDIVIDUAL_VALUE)
                value = MIN_INDIVIDUAL_VALUE;
            else if(value > MAX_INDIVIDUAL_VALUE)
                value = MAX_INDIVIDUAL_VALUE;

            IndividualValue = value;

            return Recalculate();
        }

        public const int MIN_EFFORT_VALUE = 0;
        public const int MAX_EFFORT_VALUE = 255;
        public int AddEffortValue(int value)
        {
            EffortValue += value;
            if (EffortValue < MIN_EFFORT_VALUE)
                EffortValue = MIN_EFFORT_VALUE;
            else if (EffortValue > MAX_EFFORT_VALUE)
                EffortValue = MAX_EFFORT_VALUE;

            return Recalculate();
        }
    }

    public class ContainerStat : Stat
    {
        public ContainerStat(Names name, int b) : base(name, b)
        {

        }
        public ContainerStat(Names name, int b, int iv, int ev) : base(name, b, iv, ev)
        {

        }

        public override int CalculateMaxStat(int level)
        {
            this._LastKnownLevel = level;

            var output = (int)(Math.Floor(((2 * Base + IndividualValue + Math.Floor(EffortValue / 4.0)) * level) / 100) + level + 10);

            return output;
        }

        public override int CalculateCurrentStat(int level)
        {
            this._LastKnownLevel = level;
            var currentPercentage = Current / (Max * 1.0);
            var output = (int)Math.Ceiling(Max * currentPercentage);

            return output;
        }

        public override int Recalculate()
        {
            this.Max = CalculateMaxStat();
            this.Current = CalculateCurrentStat();

            return Current;
        }

        public bool HasMoreThanAmount(int amount)
        {
            return Current >= amount;
        }

        public bool LoseFromCurrent(int amount)
        {
            Current -= amount;

            if(Current < 0)
            {
                Current = 0;
                return true;
            }

            return false;
        }
        
        public void AddToCurrent(int amount)
        {
            Current += amount;

            if(Current > Max)
            {
                Current = Max;
            }
        }
        public void AddToFull()
        {
            Current = Max;
        }
    }

    public abstract class BaseStats
    {
        public ContainerStat Health { get; protected set; }
        public ContainerStat Mana { get; protected set; }
        public Stat Attack { get; protected set; }
        public Stat Defense { get; protected set; }
        public Stat SpecialAttack { get; protected set; }
        public Stat SpecialDefense { get; protected set; }
        public Stat Speed { get; protected set; }
    }

    public class Stats : BaseStats
    {
        public int Level { get; private set; }

        public Stats(string n, int h, int m, int a, int d, int sa, int sd, int sp)
        {
            Init(n, 1, h, 0, 0, m, 0, 0, a, 0, 0, d, 0, 0, sa, 0, 0, sd, 0, 0, sp, 0, 0);
        }

        public Stats(JSONNode baseMJson, JSONNode savedJson)
        {
            var stats = savedJson["stats"];
            var bases = baseMJson["stats"];
            Init(stats["nature"], stats["level"].AsInt,
                bases["health"].AsInt, stats["health"]["iv"].AsInt, stats["health"]["ev"].AsInt,
                bases["mana"].AsInt, stats["mana"]["iv"].AsInt, stats["mana"]["ev"].AsInt,
                bases["attack"].AsInt, stats["attack"]["iv"].AsInt, stats["attack"]["ev"].AsInt,
                bases["defense"].AsInt, stats["defense"]["iv"].AsInt, stats["defense"]["ev"].AsInt,
                bases["special_attack"].AsInt, stats["special_attack"]["iv"].AsInt, stats["special_attack"]["ev"].AsInt,
                bases["special_defense"].AsInt, stats["special_defense"]["iv"].AsInt, stats["special_defense"]["ev"].AsInt,
                bases["speed"].AsInt, stats["speed"]["iv"].AsInt, stats["speed"]["ev"].AsInt);
        }

        public void Init(string n, int l, int h, int hi, int he, int m, int mi, int me, int a, int ai, int ae, int d, int di, 
                        int de, int sa, int sai, int sae, int sd, int sdi, int sde, int sp, int spi, int spe)
        {
            this.Level = l;
            this.Health = new ContainerStat(Stat.Names.Health, h, hi, he);
            this.Mana = new ContainerStat(Stat.Names.Mana, m, mi, me);
            this.Attack = new Stat(Stat.Names.Attack, a, ai, ae);
            this.Defense = new Stat(Stat.Names.Defense, d, di, de);
            this.SpecialAttack = new Stat(Stat.Names.SpecialAttack, sa, sai, sae);
            this.SpecialDefense = new Stat(Stat.Names.SpecialDefense, sd, sdi, sde);
            this.Speed = new Stat(Stat.Names.Speed, sp, spi, spe);

            SetLevelAndNatures();
        }

        public void SetLevelAndNatures()
        {
            this.Health.AddLevelAndNature(Level);
            this.Mana.AddLevelAndNature(Level);
            this.Attack.AddLevelAndNature(Level);
            this.Defense.AddLevelAndNature(Level);
            this.SpecialAttack.AddLevelAndNature(Level);
            this.SpecialDefense.AddLevelAndNature(Level);
            this.Speed.AddLevelAndNature(Level);
        }

        public const int MAX_LEVEL = 100;
        public JSONClass AddLevel(int levelsToAdd)
        {
            Level += levelsToAdd;
            if (Level > Stats.MAX_LEVEL)
                Level = Stats.MAX_LEVEL;

            var statChanges = new JSONClass();
            statChanges.Add("Health", new JSONData(Health.AddLevel(levelsToAdd)));
            statChanges.Add("Mana", new JSONData(Mana.AddLevel(levelsToAdd)));
            statChanges.Add("Attack", new JSONData(Attack.AddLevel(levelsToAdd)));
            statChanges.Add("Defense", new JSONData(Defense.AddLevel(levelsToAdd)));
            statChanges.Add("SpecialAttack", new JSONData(SpecialAttack.AddLevel(levelsToAdd)));
            statChanges.Add("SpecialDefense",new JSONData(SpecialDefense.AddLevel(levelsToAdd)));
            statChanges.Add("Speed", new JSONData(Speed.AddLevel(levelsToAdd)));

            return statChanges;
        }
        public JSONClass AddLevel()
        {
            return AddLevel(1);
        }

        public void ResetStages()
        {
            this.Health.ClearStages();
            this.Mana.ClearStages();
            this.Attack.ClearStages();
            this.Defense.ClearStages();
            this.SpecialAttack.ClearStages();
            this.SpecialDefense.ClearStages();
            this.Speed.ClearStages();
        }

        public void FullRecover()
        {
            this.Health.AddToFull();
            this.Mana.AddToFull();
        }
    }
}
