﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTesterController : WeaponController {

    public int BasePower = 10;
    public DamageType DamageType = DamageType.Physical;
    public float BaseAttackTime = 50;
    public float BaseMuzzleVelocity = 30f;
    public float BaseReloadTime = 0.6f;
    public int BaseAmmoAmount = 10;

    public override void EquipWeapon(Weapon weaponToEquip)
    {
        if (EquippedWeapon != null)
        {
            Destroy(EquippedWeapon.gameObject);
        }

        EquippedWeapon = Instantiate(weaponToEquip, WeaponHold.position, WeaponHold.rotation) as Weapon;
        EquippedWeapon.Initialize(new WeaponStats() {
            BasePower = this.BasePower,
            DamageType = this.DamageType,
            BaseAttackTime = this.BaseAttackTime,
            BaseMuzzleVelocity = this.BaseMuzzleVelocity,
            BaseAmmoAmount = this.BaseAmmoAmount,
            BaseReloadTime = this.BaseReloadTime,
            OwnerStats = this.OwnerStats
        }, OwnerStats);
        EquippedWeapon.transform.parent = WeaponHold;
    }

}
