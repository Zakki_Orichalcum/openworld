﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDungeonRoom {

    List<IDungeonRoom> GetConnectedRooms();
}
