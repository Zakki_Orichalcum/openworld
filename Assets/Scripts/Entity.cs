﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

public class Entity : MonoBehaviour, IDamageable
{
    public Stats Stats { get; set; }

    public bool IsDead {get;set;}
    public event System.Action OnDeath;

    protected virtual void Start()
    {

    }

    public virtual void Initialize(Stats stat)
    {
        this.Stats = stat;
    }

    public virtual bool DamageWillKill(Damage damage)
    {
        return (CalculateDamage(Stats, damage) >= Stats.Health.Current) ;
    }

    public virtual void TakeHit(Damage damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        TakeDamage(damage);
    }

    public virtual void TakeDamage(Damage damage)
    {
        var calculatedDamage = CalculateDamage(Stats, damage);

        var isDead = Stats.Health.LoseFromCurrent(calculatedDamage);

        if(isDead)
        {
            Die();
        }
    }

    public void Die()
    {
        if (!IsDead)
        {
            IsDead = true;

            if (OnDeath != null)
            {
                OnDeath();
            }

            GameObject.Destroy(gameObject);
        }
    }

    public static Stats GetDefaultStats()
    {
        return new Stats("Default", 10, 10, 10, 10, 10, 10, 10);
    }

    public static int CalculateDamage(Stats receiverStats, Damage damage)
    {
        var defenseValue = ProperStatValues(receiverStats, damage);
        var modifier = Random.Range(85, 101) / 100f;

        return (int)((((2 * damage.DamagerStats.Level + 10) / 250) * (defenseValue) * damage.BasePower + 2) * modifier);
    }
    private static float ProperStatValues(Stats receiverStats, Damage damage)
    {
        switch (damage.DamageType)
        {
            case (DamageType.Physical):
                return damage.DamagerStats.Attack.Current / receiverStats.Defense.Current * 1.0f;
            case (DamageType.Magical):
                return damage.DamagerStats.SpecialAttack.Current / receiverStats.SpecialDefense.Current * 1.0f;
            case(DamageType.True):
            default:
                return 1f;
        }
    }
}
