﻿using UnityEngine;

public interface IShipController {

	// Use this for initialization
    void Move(Vector2 vector, float speed);

    void MoveBasedOnDamage();
}
