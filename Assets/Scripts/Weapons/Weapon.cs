﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

public enum WeaponStates
{
    Idle,
    Attacking,
    Reloading,
    Inactive
}

public abstract class Weapon : MonoBehaviour, IWeapon
{
    public Transform Muzzle;
    public Projectile Projectile;
    float maxReloadAngle = 60;

    public WeaponStats WeaponStats { get; set; }
    public Stats OwnerStats { get; set; }
    public WeaponStates WeaponState { get; set; }
    public int CurrentAmmoAmount { get; set; }
    public Quaternion InitialMuzzleRotation { get; set; }

    public virtual void Initialize(Stats ownerStats)
    {
        InitialMuzzleRotation = Muzzle.localRotation;
        var weaponStats = new WeaponStats() { BasePower = 20, DamageType = DamageType.Physical, BaseAttackTime = 100, 
                                                BaseMuzzleVelocity = 35, BaseAmmoAmount = 24, BaseReloadTime = 1.5f, 
                                                OwnerStats = this.OwnerStats };
        Initialize(weaponStats, ownerStats);
    }

    public virtual void Initialize(WeaponStats stats, Stats ownerStats)
    {
        this.OwnerStats = ownerStats;
        this.WeaponStats = stats;
        this.WeaponState = WeaponStates.Idle;
        this.CurrentAmmoAmount = WeaponStats.GetMaxAmmoAmount();
    }

    float nextShotTime;

    public virtual void Attack()
    {
        if (AbleToAttack())
        {
            WeaponState = WeaponStates.Attacking;

            nextShotTime = Time.time + WeaponStats.GetAttackTime() / 1000;
            Projectile newProjectile = Instantiate(Projectile, Muzzle.position, Muzzle.rotation) as Projectile;
            newProjectile.Initialize(OwnerStats, WeaponStats.BasePower, WeaponStats.DamageType, ProjectileDestroyed);
            newProjectile.SetSpeed(WeaponStats.GetMuzzleVelocity());

            ReduceAmmo();
            WeaponState = WeaponStates.Idle;
        }
    }

    public bool AbleToAttack()
    {
        return WeaponState == WeaponStates.Idle && !NeedsToReload() && Time.time > nextShotTime;
    }


    public virtual void ProjectileDestroyed()
    {
        //Not implemented for this weapon
    }

    public virtual void Reload()
    {
        if(AbleToReload())
        {
            StartCoroutine(AnimateReload());
        }
    }

    IEnumerator AnimateReload()
    {
        WeaponState = WeaponStates.Reloading;
        yield return new WaitForSeconds(0.2f);

        float reloadSpeed = 1f / WeaponStats.GetReloadTime();
        float percent = 0;
        Vector3 initialRot = transform.localEulerAngles;

        while (percent < 1)
        {
            percent += Time.deltaTime * reloadSpeed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            float reloadAngle = Mathf.Lerp(0, maxReloadAngle, interpolation);
            transform.localEulerAngles = initialRot + Vector3.left * reloadAngle;

            yield return null;
        }

        WeaponState = WeaponStates.Idle;
        CurrentAmmoAmount = WeaponStats.GetMaxAmmoAmount();
    }

    public bool AbleToReload()
    {
        return WeaponState == WeaponStates.Idle && HasAmmo() && CurrentAmmoAmount < WeaponStats.GetMaxAmmoAmount();
    }

    public bool HasAmmo()
    {
        return WeaponStats.GetMaxAmmoAmount() > 0;
    }

    public bool NeedsToReload()
    {
        return HasAmmo() && CurrentAmmoAmount <= 0;
    }

    public void ReduceAmmo()
    {
        if(HasAmmo())
        {
            CurrentAmmoAmount--;
        }
    }
}

public struct WeaponStats
{
    public int BasePower { get; set; }
    public DamageType DamageType { get; set; }
    public float BaseAttackTime { get; set; }
    public float BaseMuzzleVelocity { get; set; }
    public float BaseReloadTime { get; set; }
    public int BaseAmmoAmount { get; set; }
    public Stats OwnerStats { get; set; }

    public void UpdateOwnerStats(Stats ownerStats)
    {
        this.OwnerStats = ownerStats;
    }

    public float GetMuzzleVelocity()
    {
        return BaseMuzzleVelocity;
    }

    public float GetAttackTime()
    {
        return BaseAttackTime;
    }

    public float GetReloadTime()
    {
        return BaseReloadTime;
    }

    public int GetMaxAmmoAmount()
    {
        return BaseAmmoAmount;
    }
}
