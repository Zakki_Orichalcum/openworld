﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

public interface IDamageable {

    void TakeHit(Damage damage, Vector3 hitPoint, Vector3 hitDirection);

    void TakeDamage(Damage damage);

}

public enum DamageType{
    Physical,
    Magical,
    True
}

public struct Damage
{
    public int BasePower { get; set; }
    public DamageType DamageType { get; set; }
    public Stats DamagerStats { get; set; }

    public override string ToString()
    {
        return string.Format("BasePower : {0}, DamageType : {1}, DamagerStats : {{ {2} }}", BasePower, DamageType, DamagerStats.ToString());
    }
}
