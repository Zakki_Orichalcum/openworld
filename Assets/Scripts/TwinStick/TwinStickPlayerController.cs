﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class TwinStickPlayerController : MonoBehaviour
{

    private Rigidbody myRigidBody { get; set; }
    private Vector3 Velocity { get; set; }

    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
        myRigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    public void Move(Vector3 vel)
    {
        Velocity = vel;
    }

    public void LookAt(Vector3 point)
    {
        transform.LookAt(new Vector3(point.x, transform.position.y, point.z));
    }

    void FixedUpdate()
    {
        myRigidBody.MovePosition(myRigidBody.position + Velocity * Time.fixedDeltaTime);
    }
}
