﻿using System;
using System.Text;

namespace RPG.Utility
{
    public class DateAndTime
    {
        public static DateTime SafeDate(string stringIn, DateTime defaultVal)
        {
            DateTime outVal;
            return (DateTime.TryParse(stringIn, out outVal)) ? outVal : defaultVal;
        }
        public static DateTime? SafeDateNullable(string stringIn)
        {
            DateTime outVal;
            return (DateTime.TryParse(stringIn, out outVal)) ? (DateTime?)outVal : null;
        }
        public static string UserFriendlyTime(decimal totalSeconds, bool shortLabels = false)
        {
            return UserFriendlyTime((int)Math.Round(totalSeconds, 0), shortLabels);
        }
        public static string UserFriendlyTime(int totalSeconds, bool shortLabels = false)
        {
            var timeSpan = TimeSpan.FromSeconds(totalSeconds);

            var days = timeSpan.Days;
            var hours = timeSpan.Hours;
            var minutes = timeSpan.Minutes;
            var seconds = timeSpan.Seconds;

            var sb = new StringBuilder();
            if (!shortLabels)
            {
                if (days > 0)
                    sb.AppendFormat("{0} day{1}, ", days, (days > 1) ? "s" : "");
                if (hours > 0)
                    sb.AppendFormat("{0} hour{1}, ", hours, (hours > 1) ? "s" : "");
                if (minutes > 0)
                    sb.AppendFormat("{0} minute{1}{2}", minutes, (minutes > 1) ? "s" : "", (days == 0) ? ", " : "");
                if (days == 0)
                    sb.AppendFormat("{0} second{1}", seconds, (seconds > 1) ? "s" : "");
            }
            else
            {
                if (days > 0)
                    sb.AppendFormat("{0}d, ", days);
                if (hours > 0)
                    sb.AppendFormat("{0}h, ", hours);
                if (minutes > 0)
                    sb.AppendFormat("{0}m{1}", minutes, (days == 0) ? ", " : "");
                if (days == 0)
                    sb.AppendFormat("{0}s", seconds);
            }

            return sb.ToString();
        }

        public static int TimeUnitToMinutesMultiplier(string unit)
        {
            return (unit == "h") ? 60 : (unit == "d") ? 1440 : (unit == "w") ? 10080 : 1;
        }
    }
}
