﻿using UnityEngine;
using System.Collections;

public class CrosshairController : MonoBehaviour {

    private SpriteRenderer mySpriteRenderer;

    public SpriteRenderer dot;
    public LayerMask targetMask;
    public Color IdleDotColor;
    public Color EnemyDotColor;
    public Sprite DefaultCrosshairs;
    public Sprite EnemyCrosshairs;

    private float RotationSpeed = 40;
    private bool targetsToggle = false;

    void Start()
    {
        //Cursor.visible = false;
        IdleDotColor = dot.color;
        this.mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

	// Update is called once per frame
	void Update () {
        Rotate();
	}

    public void DetectTargets(Ray ray)
    {
        if (Physics.Raycast(ray, 100, targetMask))
        {
            dot.color = EnemyDotColor;
            if (!targetsToggle)
            {
                mySpriteRenderer.sprite = EnemyCrosshairs;
                RotationSpeed = 80;
                targetsToggle = !targetsToggle;
            }
        }
        else
        {
            dot.color = IdleDotColor;
            if (targetsToggle)
            {
                mySpriteRenderer.sprite = DefaultCrosshairs;
                RotationSpeed = 40;
                targetsToggle = !targetsToggle;
            }
        }
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.forward * -RotationSpeed * Time.deltaTime);
    }
}
