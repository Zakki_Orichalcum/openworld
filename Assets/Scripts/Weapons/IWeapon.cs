﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

public interface IWeapon {

    void Initialize(Stats ownerStats);

    void Attack();
    bool AbleToAttack();
    void Reload();
    bool AbleToReload();
    void ProjectileDestroyed();

    bool HasAmmo();
    void ReduceAmmo();
    bool NeedsToReload();
}
