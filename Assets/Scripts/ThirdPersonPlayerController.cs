﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponController))]
[RequireComponent(typeof(Entity))]
public class ThirdPersonPlayerController : GravityEffectCharacter {

    public Transform BodyTarget;
    public Transform AimingTarget;

    public float WalkSpeed = 2;
    public float RunSpeed = 6;
    public float JumpHeight = 1;
    public float AimingSpeed = 2;
    public bool DebugAiming = false;

    public float TurnSmoothTime = 0.2f;
    private float TurnSmoothVelocity;

    public float SpeedSmoothTime = 0.1f;
    private float SpeedSmoothVelocity;

    private bool wasLastAiming = false;

    private Animator Animator { get; set; }
    private Transform Spine { get; set; }
    private ThirdPersonCamera ThirdPersonCamera { get; set; }
    private WeaponController WeaponController { get; set; }
    private Entity PlayerEntity { get; set; }

	// Use this for initialization
	void Start () {
        Animator = GetComponent<Animator>();
        Spine = Animator.GetBoneTransform(HumanBodyBones.Spine);
        ThirdPersonCamera = Camera.main.GetComponent<ThirdPersonCamera>();
        ThirdPersonCamera.Init(BodyTarget, AimingTarget);
        Controller = GetComponent<CharacterController>();
        WeaponController = GetComponent<WeaponController>();

        PlayerEntity = GetComponent<Entity>();
        PlayerEntity.Initialize(Entity.GetDefaultStats());

        WeaponController.Initialize(PlayerEntity.Stats);
	}
	
	// Update is called once per frame
	void Update () {

        //input
        //Changes input collection eventually
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        //TODO : Change input collection eventually
        var aiming = Input.GetMouseButton(1) || DebugAiming;
        var running = Input.GetKey(KeyCode.LeftShift) && !aiming;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        ThirdPersonCamera.SetAimingMode(aiming);
        if (aiming)
        {

            WeaponController.ShowWeapon();
            if (Input.GetMouseButtonDown(0))
            {
                WeaponController.Attack();
            }
        }
        else
        {
            WeaponController.HideWeapon();
        }

        Rotation(input.normalized, aiming);
        Move(input.normalized, running, aiming);

        //animation
        float animationSpeedPercent = (running) ? CurrentSpeed / RunSpeed : CurrentSpeed / WalkSpeed * 0.5f;
        Animator.SetFloat("SpeedPercent", animationSpeedPercent, GetModifiedSmoothTime(SpeedSmoothTime), Time.deltaTime);
        Animator.SetBool("RangedWeaponActive", aiming);
	}

    protected void Move(Vector2 input, bool running, bool aiming)
    {
        var targetSpeed = ((aiming) ? AimingSpeed : (running) ? RunSpeed : WalkSpeed) * input.magnitude;
        var movementSpeed = Mathf.SmoothDamp(CurrentSpeed, targetSpeed, ref SpeedSmoothVelocity, GetModifiedSmoothTime(SpeedSmoothTime));

        base.Move(movementSpeed);

        SetCurrentSpeed(new Vector2(Controller.velocity.x, Controller.velocity.z).magnitude);
    }

    private void Jump()
    {
        if(Controller.isGrounded)
        {
            float jumpVelocity = Mathf.Sqrt(-2 * GRAVITATIONAL_CONSTANT * JumpHeight);

            SetVelocityY(jumpVelocity);
        }
    }

    private void Rotation(Vector2 input, bool aiming)
    {
        if (aiming)
        {
            //var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //var plane = new Plane(AimingTarget.transform.forward * -1, AimingTarget.transform.forward * 10);
            //float rayDistance;

            //if (plane.Raycast(ray, out rayDistance))
            //{
            //    var point = ray.GetPoint(rayDistance);
            //    var eulerAngles = Mathf.Atan2(point.x, point.z) * Mathf.Rad2Deg;
            //    transform.eulerAngles = Vector3.up * eulerAngles;

            //    AimWeapon(point);
            //}

            var cameraPitch = -ThirdPersonCamera.Pitch;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y,
                                                                        ThirdPersonCamera.transform.eulerAngles.y,
                                                                        ref TurnSmoothVelocity,
                                                                        TurnSmoothTime);

            if (!wasLastAiming)
                wasLastAiming = true;
        }
        else
        {
            if (input == Vector2.zero)
                return;

            var targetRotation = Mathf.Atan2(input.x, input.y) * Mathf.Rad2Deg + ThirdPersonCamera.transform.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y,
                                                                        targetRotation,
                                                                        ref TurnSmoothVelocity,
                                                                        GetModifiedSmoothTime(TurnSmoothTime));

            if (wasLastAiming)
            {
                wasLastAiming = false;
                ResetWeaponAim();
            }
                
        }
    }

    private void AimWeapon(float pitch)
    {
        Spine.eulerAngles = Vector3.right * pitch;
        WeaponController.AimWeapon(pitch);
    }

    private void ResetWeaponAim()
    {
        WeaponController.ResetWeaponAim();
    }
}
