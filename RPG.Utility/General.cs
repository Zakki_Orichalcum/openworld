﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    //This is currently a Class for classes that should have their own file but at too small to really warrent it yet

    public class General
    {
        public static Guid SafeGuid(string stringIn)
        {
            return SafeGuid(stringIn, Guid.Empty);
        }
        public static Guid SafeGuid(string stringIn, Guid defaultVal)
        {
            Guid outVal;
            return (Guid.TryParse(stringIn, out outVal)) ? outVal : defaultVal;
        }
        public static bool SafeBool(string strIn, bool defaultVal = false)
        {
            if (string.IsNullOrEmpty(strIn))
                return defaultVal;

            if (strIn.Equals("On", StringComparison.CurrentCultureIgnoreCase)) strIn = "true";
            if (strIn.Equals("Off", StringComparison.CurrentCultureIgnoreCase)) strIn = "false";

            bool returnOut;
            return (bool.TryParse(strIn, out returnOut)) ? returnOut : defaultVal;
        }

        #region CLONING
        public static List<T> CloneIntList<T>(List<T> intList)
        {
            var retArray = new T[intList.Count];
            for (var i = 0; i < intList.Count; i++)
            {
                retArray[i] = intList[i];
            }

            return retArray.ToList();
        }
        #endregion

        #region FILES
        public static string GetContentType(string extension)
        {
            switch (extension.ToLower())
            {
                case ".gif": return "image/gif";
                case ".jpg": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".png": return "image/png";
                case ".swf": return "application/x-shockwave-flash";
                case ".wmv": return "video/x-ms-wmv";
                case ".xaml": return "application/xaml+xml";
                case ".mp4": return "video/mp4	";
                case ".mov": return "video/quicktime";
                case ".pdf": return "application/pdf";
                case ".ppt": return "application/vnd.ms-powerpoint";
                case ".pptx": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case ".doc": return "application/msword";
                case ".docx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".xls": return "application/vnd.ms-excel";
                case ".xlsx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".htm": return "text/html";
                case ".html": return "text/html";

                default: throw new Exception("Content type is not supported in call to GetContentType");
            }
        }

        #endregion

        #region RANDOM

        public static bool RandomBoolBasedOnOdds(double oddsOutOfOne)
        {
            var newRand = new Random().NextDouble();
            return (newRand <= oddsOutOfOne);
        }

        #endregion
    }
}
