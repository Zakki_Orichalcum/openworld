﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RPG.Utility;

public class ThirdPersonCamera : MonoBehaviour {

    public Transform NormalTarget;
    public Transform AimingTarget;

    public float MouseSensitivity = 10;
    public float DistanceFromTarget = 2;

    public MinMax NormalPitchMinMax = new MinMax(-40, 85);
    public MinMax AimingPitchMinMax = new MinMax(-15, 20);

    public float rotatonSmoothTime = 0.12f;
    public float AimRotationSmoothTime = 0.5f;
    private Vector3 rotationSmoothVelocity;
    private Vector3 currentRotation;

    public float Yaw { get; set; }
    public float Pitch { get; set; }

    private bool isAimingMode { get; set; }

    public void Init(Transform normalTarget, Transform aimingTarget)
    {
        this.NormalTarget = normalTarget;
        this.AimingTarget = aimingTarget;
    }

    // Update is called once per frame
    void LateUpdate () {

        Yaw += Input.GetAxis("Mouse X") * MouseSensitivity;
        Pitch -= Input.GetAxis("Mouse Y") * MouseSensitivity;

        if (isAimingMode)
            AimingMode();
        else
            NormalMode();
	}

    private void AimingMode()
    {
        Pitch = Mathf.Clamp(Pitch, AimingPitchMinMax.Min, AimingPitchMinMax.Max);

        currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(Pitch, Yaw), ref rotationSmoothVelocity, AimRotationSmoothTime);
        transform.eulerAngles = currentRotation;

        transform.position = AimingTarget.position;
    }

    private void NormalMode()
    {
        Pitch = Mathf.Clamp(Pitch, NormalPitchMinMax.Min, NormalPitchMinMax.Max);

        currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(Pitch, Yaw), ref rotationSmoothVelocity, rotatonSmoothTime);
        transform.eulerAngles = currentRotation;

        transform.position = NormalTarget.position - transform.forward * DistanceFromTarget;
    }

    public void SetAimingMode(bool aiming)
    {
        isAimingMode = aiming;
    }
}
