﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

[RequireComponent(typeof(WeaponController))]
[RequireComponent(typeof(TwinStickPlayerController))]
public class TwinStickPlayer : Entity {

    public CrosshairController Crosshairs;

    private WeaponController WeaponController { get; set; }
    private TwinStickPlayerController Controller { get; set; }
    private Camera MainCamera { get; set; }

    public int MoveSpeed = 5;

	protected override void Start () {
        WeaponController = GetComponent<WeaponController>();
        Controller = GetComponent<TwinStickPlayerController>();
        MainCamera = Camera.main;

        //Don't have this here
        var playerStats = new Stats("Default", 10, 10, 10, 10, 10, 10, 10);
        Initialize(playerStats);
	}
	
    public override void Initialize(Stats stat)
    {
 	    base.Initialize(stat);
        WeaponController.Initialize(stat);
    }

	void Update () {
        MovementInput();

        LookAtInput();

        AttackInput();
	}

    void MovementInput()
    {
        var moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        var moveVelocity = moveInput.normalized * MoveSpeed;
        Controller.Move(moveVelocity);
    }

    void LookAtInput()
    {
        var ray = MainCamera.ScreenPointToRay(Input.mousePosition);
        var groundPlane = new Plane(Vector3.up, Vector3.down * WeaponController.WeaponHeight);
        float rayDistance;

        if(groundPlane.Raycast(ray, out rayDistance))
        {
            var point = ray.GetPoint(rayDistance);
            Controller.LookAt(point);
            Crosshairs.transform.position = point;
            Crosshairs.DetectTargets(ray);
        }
    }

    void AttackInput()
    {
        if (Input.GetMouseButton(0))
        {
            WeaponController.Attack();
        }
    }
}
