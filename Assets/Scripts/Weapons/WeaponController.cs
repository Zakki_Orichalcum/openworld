﻿using UnityEngine;
using System.Collections;
using RPG.Data.Statistics;

public class WeaponController : MonoBehaviour {

    public Weapon StartingWeapon;
    public Transform WeaponHold;

    protected Weapon EquippedWeapon { get; set; }
    public Stats OwnerStats { get; set; }

    public float WeaponHeight
    {
        get { return WeaponHold.position.y; }
    }

    public void Initialize(Stats ownerStats)
    {
        this.OwnerStats = ownerStats;
        EquipDefault();
    }

    private void EquipDefault()
    {
        if (StartingWeapon != null)
        {
            EquipWeapon(StartingWeapon);
        }
    }

    public virtual void EquipWeapon(Weapon weaponToEquip)
    {
        if (EquippedWeapon != null)
        {
            Destroy(EquippedWeapon.gameObject);
        }

        EquippedWeapon = Instantiate(weaponToEquip, WeaponHold.position, WeaponHold.rotation) as Weapon;
        EquippedWeapon.Initialize(OwnerStats);
        EquippedWeapon.transform.parent = WeaponHold;
    }

    public void Attack()
    {
        if (EquippedWeapon != null)
        {
            if(EquippedWeapon.NeedsToReload())
            {
                Reload();
            }
            else
            {
                EquippedWeapon.Attack();
            }
        }
    }

    public void Reload()
    {
        if(EquippedWeapon != null && EquippedWeapon.HasAmmo())
        {
            EquippedWeapon.Reload();
        }
    }

    public void HideWeapon()
    {
        EquippedWeapon.enabled = false;
    }

    public void ShowWeapon()
    {
        EquippedWeapon.enabled = true;
    }

    public void AimWeapon(float pitch)
    {
        EquippedWeapon.Muzzle.eulerAngles = Vector3.right * pitch;
    }

    public void ResetWeaponAim()
    {
        EquippedWeapon.Muzzle.localRotation = EquippedWeapon.InitialMuzzleRotation;
    }
}
