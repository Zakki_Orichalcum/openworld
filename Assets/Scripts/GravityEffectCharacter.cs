﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public abstract class GravityEffectCharacter : MonoBehaviour {

    public const float GRAVITATIONAL_CONSTANT = -10;

    [Range(0, 1)]
    public float AirControlPercent;

    public float CurrentSpeed { get; private set; }
    public float VelocityY { get; private set; }

    protected CharacterController Controller { get; set; }

    // Use this for initialization
    void Start () {
        Controller = GetComponent<CharacterController>();
    }

    public bool IsGrounded()
    {
        return Controller.isGrounded;
    }

    public virtual void Move(float movementSpeed)
    {
        CurrentSpeed = movementSpeed;

        VelocityY += Time.deltaTime * GRAVITATIONAL_CONSTANT;
        Vector3 velocity = transform.forward * CurrentSpeed + Vector3.up * VelocityY;

        Controller.Move(velocity * Time.deltaTime);

        if (IsGrounded())
        {
            VelocityY = 0;
        }
    }

    protected float GetModifiedSmoothTime(float smoothTime)
    {
        if (Controller.isGrounded)
        {
            return smoothTime;
        }

        return smoothTime / AirControlPercent;
    }

    protected void SetVelocityY(float value)
    {
        VelocityY = value;
    }

    protected void SetCurrentSpeed(float value)
    {
        CurrentSpeed = value;
    }
}
