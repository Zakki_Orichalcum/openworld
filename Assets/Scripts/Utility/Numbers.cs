﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RPG.Utility
{
    public class Numbers
    {
        public static int SafeInt(string stringIn, int defaultVal = -1)
        {
            int outVal;
            return (Int32.TryParse(stringIn, out outVal)) ? outVal : defaultVal;
        }
        public static double SafeDouble(string strIn, double defaultVal = -1)
        {
            if (string.IsNullOrEmpty(strIn))
                return defaultVal;

            double returnOut;
            return (double.TryParse(strIn, out returnOut)) ? returnOut : defaultVal;
        }
        public static double SafeProbabilityBaseOnBool(string strIn, double defaultOut = 0)
        {
            if (string.IsNullOrEmpty(strIn))
                return defaultOut;

            switch (strIn.ToLower())
            {
                case "on":
                case "true":
                    return 1;
                case "off":
                case "false":
                    return 0;
                case "half":
                    return .5;

                default:
                    var doubleVal = SafeDouble(strIn, defaultOut);
                    if (doubleVal > 1 && doubleVal <= 100)
                        doubleVal = doubleVal / (double)100;

                    return doubleVal;
            }
        }

        public static List<int> SafeIntList(IEnumerable<string> stringsIn, int defaultValue = -1)
        {
            return stringsIn.Select(s => SafeInt(s)).Where(safeInt => safeInt >= 0).ToList();
        }
        public static List<int> CloneIntList(List<int> intList)
        {
            var retArray = new int[intList.Count];
            for (var i = 0; i < intList.Count; i++)
            {
                retArray[i] = intList[i];
            }

            return retArray.ToList();
        }
    }

    public struct MinMax
    {
        public float Min { get; set; }
        public float Max { get; set; }

        public MinMax(float min, float max)
        {
            this.Min = min;
            this.Max = max;
        }
    }
}
