﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace RPG.Utility
{
    public static class Strings
    {
        #region MATCHING AND GET BETWEEN

        public const string START_SIGNIFIER = "ST4RT";
        public const string END_SIGNIFIER = "3ND1NG";

        #region GET MATCHES (LAZY)

        public static Match GetMatchBetween(string source, string start, string end, int startingPoint = 0)
        {
            var match = new Match();
            var checkIndex = source.IndexOf(start, startingPoint);
            match.BeginningTag = start;
            match.EndingTag = end;
            match.StartIndex = (checkIndex > -1) ? checkIndex + start.Length : -1;
            match.EndIndex = source.IndexOf(end, match.StartIndex);

            if (match.ContainsStart() && match.ContainsEnd())
            {
                match.Content = source.Substring(match.StartIndex, match.EndIndex - match.StartIndex);
            }
            else if (match.ContainsStart())
                match = GetMatchFromPartToTheEnd(source, start, end, startingPoint);
            else if (match.ContainsEnd())
                match = GetMatchFromBeginningToPart(source, end, start, startingPoint);

            return match;
        }
        public static Match GetMatchBetween(string source, string[] starts, string[] ends, int startingPoint = 0)
        {
            Match earliestMatch = new Match();
            earliestMatch.Max();

            foreach (var start in starts)
            {
                foreach (var end in ends)
                {
                    var nextMatch = GetMatchBetween(source, start, end, startingPoint);

                    if (nextMatch.ContainsStart() && nextMatch.StartIndex < earliestMatch.StartIndex)
                        earliestMatch = nextMatch;
                    else if (nextMatch.ContainsEnd() && nextMatch.StartIndex <= earliestMatch.StartIndex && nextMatch.EndIndex < earliestMatch.EndIndex)
                        earliestMatch = nextMatch;
                }
            }

            return earliestMatch;
        }
        public static Match GetMatchBetween(string source, string start, string[] ends, int startingPoint = 0)
        {
            return GetMatchBetween(source, new string[] { start }, ends, startingPoint);
        }
        public static Match GetMatchBetween(string source, string[] starts, string end, int startingPoint = 0)
        {
            return GetMatchBetween(source, starts, new string[] { end }, startingPoint);
        }

        public static Match GetMatchFromPartToTheEnd(string source, string start, string originalEnd = "", int startingPoint = 0)
        {
            var match = GetMatchBetween(source + END_SIGNIFIER, start, END_SIGNIFIER, startingPoint);
            match.SetOriginalTags(start, (string.IsNullOrEmpty(originalEnd)) ? END_SIGNIFIER : originalEnd);

            return match;
        }

        public static Match GetMatchFromBeginningToPart(string source, string end, string originalStart = "", int startingPoint = 0)
        {
            var match = GetMatchBetween(source.Insert(startingPoint, START_SIGNIFIER), START_SIGNIFIER, end, startingPoint);
            match.SetOriginalTags((string.IsNullOrEmpty(originalStart)) ? START_SIGNIFIER : originalStart, end);

            return match;
        }
        #endregion

        #region GET MATCHES (RIGOROUS)

        public static Match GetMatchBetweenAMatchingAndClosingTag(string source, string start, string end, int startingPoint = 0)
        {
            var match = new Match() { BeginningTag = start, EndingTag = end };
            var toMatchFromStart = "";
            var toMatchFromEnd = "";

            var indexOfStart = source.IndexOf(start, startingPoint);
            if (indexOfStart == -1)
                return null;

            var numberOfMatchedStarts = 1;
            var numberOfMatchedEnds = 0;
            var indexOfEnd = 0;
            var index = match.StartIndex = indexOfStart + start.Length;
            while (numberOfMatchedStarts != numberOfMatchedEnds && index < source.Length)
            {
                var c = source[index];
                if (start.Contains(toMatchFromStart + c))
                {
                    toMatchFromStart += c;
                    if (toMatchFromStart.Equals(start))
                    {
                        toMatchFromStart = "";
                        numberOfMatchedStarts++;
                    }
                }

                if (end.Contains(toMatchFromEnd + c))
                {
                    toMatchFromEnd += c;
                    if (toMatchFromEnd.Equals(end))
                    {
                        toMatchFromEnd = "";
                        numberOfMatchedEnds++;
                        indexOfEnd = index - end.Length;
                    }
                }

                index++;
            }

            if (numberOfMatchedStarts != numberOfMatchedEnds)
                return null;

            match.EndIndex = indexOfEnd;
            match.Content = source.Substring(match.StartIndex, match.EndIndex - match.StartIndex);

            return match;
        }

        #endregion

        #region GET STRINGS
        public static string GetStringBetween(string source, string start, string end, int startingPoint = 0)
        {
            return GetMatchBetween(source, start, end, startingPoint).ToString();
        }
        public static string GetStringBetween(string source, string[] starts, string[] ends, int startingPoint = 0)
        {
            return GetMatchBetween(source, starts, ends, startingPoint).ToString();
        }
        public static string GetStringBetween(string source, string start, string[] ends, int startingPoint = 0)
        {
            return GetMatchBetween(source, start, ends, startingPoint).ToString();
        }
        public static string GetStringBetween(string source, string[] starts, string end, int startingPoint = 0)
        {
            return GetMatchBetween(source, starts, end, startingPoint).ToString();
        }

        public static string GetStringFromPartToTheEnd(string source, string start, string originalEnd = "", int startingPoint = 0)
        {
            return GetMatchFromPartToTheEnd(source, start, originalEnd, startingPoint).ToString();
        }

        public static string GetStringFromBeginningToPart(string source, string end, string originalStart = "", int startingPoint = 0)
        {
            return GetMatchFromBeginningToPart(source, end, originalStart, startingPoint).ToString();
        }
        #endregion

        #endregion

        #region PARSING
        public static int TimeStringToInt(string timeStringIn)
        {
            var timeString = timeStringIn.Replace(".", string.Empty).Replace(":", string.Empty).ToLower().Trim();
            int firstTime = GetFirstIntFromStringStartingAt(timeString, 0);
            if (firstTime < 30)
                firstTime = firstTime * 100;  //for scenarios where they've entered something like '7 PM' rather than '7:00 PM'

            if (firstTime == 0)
                return 0;

            int indexOfAM = timeString.IndexOf("am"), indexOfPM = timeString.IndexOf("pm");
            var timeOfDay = (indexOfAM < indexOfPM && indexOfAM > -1) ? TimeOfDay.AM : TimeOfDay.PM;
            if (timeOfDay == TimeOfDay.PM)
                firstTime = firstTime + 1200;

            //they're on eastern daylight time, but some of their events are on central (CDT, CT)
            if (timeString.Contains("cdt") || timeString.Contains("ct"))
                firstTime = firstTime - 100;

            return firstTime;
        }
        private static int GetFirstIntFromStringStartingAt(string stringIn, int index)
        {
            if (string.IsNullOrEmpty(stringIn))
                return 0;

            var sb = new StringBuilder();
            while (index < stringIn.Length && Char.IsDigit(stringIn[index]))
            {
                sb.Append(stringIn[index]);
                index++;
            }

            int intOut;
            return int.TryParse(sb.ToString(), out intOut) ? intOut : 0;
        }

        public enum TimeOfDay { AM = 1, PM = 2 };

        #endregion

        #region ADDITIONAL

        public static string CamelCaseToSpaces(string stringIn, bool firstLetterToUpperCase = true)
        {
            if (string.IsNullOrEmpty(stringIn)) return string.Empty;

            if (firstLetterToUpperCase)
            {
                stringIn = Char.ToUpper(stringIn[0]) + stringIn.Substring(1);
            }

            return Regex.Replace(stringIn, "(\\B[A-Z])", " $1");
        }
        public static string GetFriendlyTitleFromCamelCaseFileName(string itemName)
        {
            var replacedTitle = itemName.Replace("-", " ").Replace("_", " ");
            var extensionlessTitle = (replacedTitle.LastIndexOf('.') > 0) ? replacedTitle.Substring(0, replacedTitle.LastIndexOf('.')) : replacedTitle;

            return extensionlessTitle;
        }

        public static string GetMD5HashForString(string stringIn)
        {
            using (var md5 = MD5.Create())
            {
                var stringBytes = Encoding.ASCII.GetBytes(stringIn);
                var md5Hash = md5.ComputeHash(stringBytes);

                return StringToHex(md5Hash);
            }
        }

        public static string StringToHex(byte[] bytes, bool upperCase = true)
        {
            var result = new StringBuilder(bytes.Length * 2);

            for (var i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
        #endregion
    }

    public class Match
    {
        public string _content;
        public string Content
        {
            get { return (_content != null) ? _content : string.Empty; }
            set { _content = value; }
        }

        private int _startIndex = -1;
        public int StartIndex
        {
            get { return (_startIndex > -1) ? _startIndex : 0; }
            set { _startIndex = value; }
        }

        private int _endIndex = -1;
        public int EndIndex
        {
            get { return (_endIndex > -1) ? _endIndex : 0; }
            set { _endIndex = value; }
        }

        private string _beginningTag;
        public string BeginningTag
        {
            get { return (_originalBeginningTag != null) ? _originalBeginningTag : (_beginningTag != null) ? _beginningTag : string.Empty; }
            set { _beginningTag = value; }
        }

        private string _endingTag;
        public string EndingTag
        {
            get { return (_originalEndingTag != null) ? _originalEndingTag : (_endingTag != null) ? _endingTag : string.Empty; }
            set { _endingTag = value; }
        }

        private string _originalBeginningTag;
        private string _originalEndingTag;

        public void SetOriginalTags(string originalBeginning, string originalEnding)
        {
            _originalBeginningTag = originalBeginning;
            _originalEndingTag = originalEnding;
        }

        public string CompleteContent
        {
            get { return BeginningTag + Content + EndingTag; }
        }

        public int Length
        {
            get { return Content.Length; }
        }
        public int TotalLength
        {
            get { return BeginningTag.Length + Length + EndingTag.Length; }
        }

        public void Max()
        {
            StartIndex = Int32.MaxValue;
            EndIndex = Int32.MaxValue;
        }

        public bool ContainsStart()
        {
            return _startIndex > -1 && ((_originalBeginningTag != null) ? _originalBeginningTag == _beginningTag : true);
        }
        public bool ContainsEnd()
        {
            return _endIndex > -1 && ((_originalEndingTag != null) ? _originalEndingTag == _endingTag : true);
        }

        public int BeginningOfStartIndex
        {
            get { return (ContainsStart()) ? StartIndex - BeginningTag.Length : 0; }
        }
        public int BeginningOfEndIndex
        {
            get { return EndIndex; }
        }

        public int CloseOfStartIndex
        {
            get { return StartIndex; }
        }
        public int CloseOfEndIndex
        {
            get { return EndIndex + EndingTag.Length; }
        }

        public override string ToString()
        {
            return Content;
        }

        public void ChangeContentWhileRetainingPlacementInformation(string newContent)
        {
            Content = newContent;
        }
    }
}
