﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using RPG.Data.Statistics;

public class Projectile : MonoBehaviour {

    public LayerMask collisionMask;
    float speed = 10;
    public Damage MyDamage;

    float lifetime = 1;
    float skinWidth = .1f;

    private Action DestroyCallback {get;set;}

    public void Initialize(Stats ownerStats, int basePower, DamageType damageType, Action destroyCallback)
    {
        this.MyDamage = new Damage() { BasePower = basePower, DamageType = damageType, DamagerStats = ownerStats };
        this.DestroyCallback = destroyCallback;
    }

    void Start()
    {
        Destroy(gameObject, lifetime);

        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, .1f, collisionMask);
        if (initialCollisions.Length > 0)
        {
            OnHitObject(initialCollisions[0], transform.position);
        }
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    void Update()
    {
        float moveDistance = speed * Time.deltaTime;
        CheckCollisions(moveDistance);
        transform.Translate(Vector3.forward * moveDistance);
    }


    void CheckCollisions(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance + skinWidth, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit.collider, hit.point);
        }
    }

    void OnHitObject(Collider c, Vector3 hitPoint)
    {
        var damageableObject = c.GetComponent<IDamageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeHit(MyDamage, hitPoint, transform.forward);
        }
        GameObject.Destroy(gameObject);
    }

    void OnDestroy()
    {
        DestroyCallback.Invoke();
    }
}
