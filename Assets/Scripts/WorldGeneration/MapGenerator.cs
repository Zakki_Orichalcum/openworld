﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utility;

/***************************************************************
 * Original by Sebastian Lague
 * 
 * Edited and Modified by John Miller
 ***************************************************************/ 

public class MapGenerator : MonoBehaviour {

    public Transform TilePrefab;
    public Transform ObstaclePrefab;
    public Transform NavmeshFloor;
    public Size MapSize;

    public float TileSize = 1;
    [Range(0, 1)]
    public float OutlinePercent;
    [Range(0, 1)]
    public float ObstaclePercent;

    public int Seed;

    void Start()
    {
        GenerateMap();
    }

    public void GenerateMap()
    {
        var holderName = "Generated Map";
        if (transform.Find(holderName))
        {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        var mapHolder = new GameObject(holderName).transform;
        mapHolder.parent = transform;

        var usableRoom = Room.GenerateRandomRoom(MapSize.Width, MapSize.Height, ObstaclePercent, Seed);

        for (var w = 0; w < usableRoom.Width; w++)
        {
            for (var h = 0; h < usableRoom.Height; h++)
            {
                var tilePosition = CoordToPosition(w, h, usableRoom.MapSize);
                if(usableRoom.TileMap[w, h] == (int)RoomTileType.Ground)
                {
                    var newTile = Instantiate(TilePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90)) as Transform;
                    newTile.localScale = Vector3.one * (1 - OutlinePercent) * TileSize;
                    newTile.parent = mapHolder;
                }
                else if(usableRoom.TileMap[w, h] == (int)RoomTileType.Obstacle)
                {
                    var newObstacle = Instantiate(ObstaclePrefab, tilePosition + Vector3.up * 0.5f, Quaternion.identity) as Transform;
                    newObstacle.localScale = Vector3.one * (1 - OutlinePercent) * TileSize;
                    newObstacle.parent = mapHolder;
                }
            }
        }
    }

    public Vector3 CoordToPosition(int x, int y, Size mapSize)
    {
        return new Vector3(-mapSize.Width / 2 + 0.5f + x, 0, -mapSize.Height / 2 + 0.5f + y) * TileSize;
    }
    public Vector3 CoordToPosition(Coord coord, Size mapSize)
    {
        return CoordToPosition(coord.x, coord.y, mapSize);
    }
}

[System.Serializable]
public struct Size
{
    public int Height;
    public int Width;
}

